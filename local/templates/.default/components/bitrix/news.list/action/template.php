<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$this->setFrameMode(true);

?>
<?foreach($arResult["ITEMS"] as $arItem):?>
<div class="sb_action">
    <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
        <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
        <img src="<?echo $arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""/>
        <?endif;?>
    </a>
    <h4><?echo $arItem["NAME"]?></h4>
    <h5><a href=""><?echo $arItem["DETAIL_TEXT"]?></a></h5>
    <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="sb_action_more"><?=GetMessage("MORE_DETAILS")?></a>
</div>

<?endforeach;
?>
