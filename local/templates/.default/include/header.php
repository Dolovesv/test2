<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 IncludeTemplateLangFile(__FILE__);?>
<div class="hd_header">
    <table>
        <tr>
            <td rowspan="2" class="hd_companyname">
                <h1><a href=""><?echo GetMessage("NAME_SHOP")?></a></h1>
            </td>
            <td rowspan="2" class="hd_txarea">
                        <span class="tel"><?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                ".default",
                                array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PATH" => "/local/templates/.default/include/phone.php"
                                ),
                                false
                            );?></span>	<br/>
                <?echo GetMessage("WORK_TIME")?> <span class="workhours"><?$APPLICATION->IncludeComponent("bitrix:main.include", "time_work_header", Array(
                        "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/local/templates/.default/include/time_work_head.php",	// Путь к файлу области
                    ),
                        false
                    );?></span>
            </td>
            <td style="width:232px">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:search.form",
                    "head",
                    Array(
                        "PAGE" => "#SITE_DIR#search/index.php",
                        "USE_SUGGEST" => "N"
                    )
                );?><br>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 11px;">
                <?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	"registr", 
	array(
		"FORGOT_PASSWORD_URL" => "/user/",
		"PROFILE_URL" => "/user/profile.php",
		"REGISTER_URL" => "/user/registration.php",
		"SHOW_ERRORS" => "Y",
		"COMPONENT_TEMPLATE" => "registr"
	),
	false
);?>
            </td>
        </tr>
    </table>
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"horizontal_multilevel1", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "horizontal_multilevel1"
	),
	false
);?><br>
</div>